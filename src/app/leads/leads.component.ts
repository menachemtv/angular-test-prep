import { Component, OnInit } from '@angular/core';
import {LeadsService} from './leads.service';
@Component({
  selector: 'jce-leads',
  templateUrl: './leads.component.html',
    styles: [`
        .users li { cursor: default; }
        .users li:hover { background: #ecf0f1; } 
        .list-group-item.active, 
        .list-group-item.active:hover, 
        .list-group-item.active:focus { 
            background-color: #ecf0f1;
            border-color: #ecf0f1; 
            color: #2c3e50;
        }
    `]
})
export class LeadsComponent implements OnInit {
  currentLead;
  leads;
  isLoading=true;
  deleteLead(lead){
this._leadsService.deletelead(lead);//add user through service file where there is an add user function.

  }
     updateLead(lead){
    this._leadsService.updatelead(lead);
  }
  constructor(
    private _leadsService: LeadsService
  ) {
 //   this.users = this._usersService.getUsers();
  }
    select(lead){
      this.currentLead= lead;
   }
  addlead(lead){
    this._leadsService.addlead(lead);//add user through service file where there is an add user function.
  }
  editLead(lead){
     this._leadsService.updatelead(lead);
   }
  ngOnInit() {
    this._leadsService.getLeads()
    .subscribe(leadsData => {this.leads=leadsData
    this.isLoading=false});
  }

}