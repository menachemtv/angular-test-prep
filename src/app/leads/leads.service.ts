import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class LeadsService {
//  private _url='http://jsonplaceholder.typicode.com/users';
leadsObservable;
  getLeads (){
//    this.usersObservable = this.af.database.list('/users')
    this.leadsObservable = this.af.database.list('/leads').map(
      leads => {
        leads.map(
          lead=> {
            lead.postTitles = [];
            for(var p in lead.posts){
              lead.postTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return leads;
      }
    );
    return this.leadsObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
  
    addlead(lead){
    this.leadsObservable.push(lead);//push is a function on array, adds object in end of array.
}
 updatelead(lead){
let leadKey = lead.$key;
let leadData = {name:lead.name,email:lead.email};
this.af.database.object('/leads/' + leadKey).update(leadData);
}
  deletelead(lead){
let leadKey = lead.$key;
this.af.database.object('/leads/' + leadKey).remove();    
   }
  constructor(private af:AngularFire) { }

}