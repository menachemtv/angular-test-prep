import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
  users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]
 //private _url = "http://jsonplaceholder.typicode.com/users";
usersObservable;

  getUsers(){
//    return this._http.get(this._url).map(res =>res.json()).delay(2000);
this.usersObservable = this.af.database.list('/users').map(
            posts => {
        posts.map(
          post=> {
            post.postUsers = [];
            for(var p in post.users){
             post.postUsers.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    );
    return this.usersObservable; 
 }
    adduser(user){
    this.usersObservable.push(user);//push is a function on array, adds object in end of array.
}
 updateuser(user){
let userKey = user.$key;
let userData = {name:user.name,email:user.email};
this.af.database.object('/users/' + userKey).update(userData);
}
  deleteuser(user){
let userKey = user.$key;
this.af.database.object('/users/' + userKey).remove();    
   }
     constructor(private af:AngularFire) { }
}
