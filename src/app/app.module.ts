
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { PostComponent } from './post/post.component';
import { UserFormComponent } from './user-form/user-form.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import{AngularFireModule} from 'angularfire2';
import { PostFormComponent } from './post-form/post-form.component';

import { UsersService } from './users/users.service';
import { PostsService } from './posts/posts.service';
import { LeadsService } from './leads/leads.service';
import { ProductsService } from './products/products.service';
import { LeadComponent } from './lead/lead.component';
import { LeadsComponent } from './leads/leads.component';
import { LeadFormComponent } from './lead-form/lead-form.component';
import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { Product1Component } from './product1/product1.component';

 export const firebaseConfig = {
    apiKey: "AIzaSyCltZoazlcfTyEe4tVSRk0r8FSINAiyPkk",
    authDomain: "angularclass-346d3.firebaseapp.com",
    databaseURL: "https://angularclass-346d3.firebaseio.com",
    storageBucket: "angularclass-346d3.appspot.com",
    messagingSenderId: "849525226262"
 }

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'leads', component: LeadsComponent },
  { path: 'products', component: ProductsComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserFormComponent,
    PostsComponent,
    PostComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent,
    LeadComponent,
    LeadsComponent,
    LeadFormComponent,
    ProductComponent,
    ProductsComponent,
    Product1Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,PostsService, LeadsService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }