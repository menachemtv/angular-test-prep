import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user';
@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  user:User;
//  Olduser:User;
  edit: Boolean=false;
  isEdit : boolean = false;//start with is edit button is false (not editing)
    editButtonText = 'Edit';//we will se Edit on button
  @Output('name') oldname: string;//create variable of name
  @Output('email') oldemail: string;//create variable of email
  @Output() editEvent = new EventEmitter<User>();//create new evnet of editing
   @Output() deleteEvent = new EventEmitter<User>()
  constructor() { }
    toggleEdit(){
this.oldname=this.user.name;
this.oldemail=this.user.email;    
this.isEdit=!this.isEdit;
this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
     this.isEdit ?  this.edit= true : this.edit= false;
     if (!this.isEdit)
     this.editEvent.emit(this.user);
  }
    cancelEdit(){
this.user.name = this.oldname;
this.user.email = this.oldemail;
this.toggleEdit();
this.edit=false;
}
sendDelete(){
     this.deleteEvent.emit(this.user);

}
  ngOnInit() {
  }

}