import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Lead} from './lead';
@Component({
  selector: 'jce-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css'],
  inputs:['lead']
})
export class LeadComponent implements OnInit {
  lead:Lead;
//  Olduser:User;
  edit: Boolean=false;
  isEdit : boolean = false;//start with is edit button is false (not editing)
    editButtonText = 'Edit';//we will se Edit on button
  @Output('name') oldname: string;//create variable of name
  @Output('email') oldemail: string;//create variable of email
  @Output() editEvent = new EventEmitter<Lead>();//create new evnet of editing
   @Output() deleteEvent = new EventEmitter<Lead>()
  constructor() { }
    toggleEdit(){
this.oldname=this.lead.name;
this.oldemail=this.lead.email;    
this.isEdit=!this.isEdit;
this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
     this.isEdit ?  this.edit= true : this.edit= false;
     if (!this.isEdit)
     this.editEvent.emit(this.lead);
  }
    cancelEdit(){
this.lead.name = this.oldname;
this.lead.email = this.oldemail;
this.toggleEdit();
this.edit=false;
}
sendDelete(){
     this.deleteEvent.emit(this.lead);

}
  ngOnInit() {
  }

}