import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  isLoading: Boolean=true;
  products;
  deleteProduct(product){
    this._productsService.deleteproduct(product);
  }

  constructor(private _productsService: ProductsService) { }

  ngOnInit() {
       this._productsService.getProducts().subscribe(postsData => 
    {this.products = postsData;
       this.isLoading =false}),
              this._productsService.getProducts1().subscribe(postsData => 
    {this.products = postsData;
       this.isLoading =false})

  }

}
