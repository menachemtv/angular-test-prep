import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {
productsObservable;
productsObservable1;

  getProducts (){
    this.productsObservable = this.af.database.list('/product/').map(
            products => {
        products.map(
          product=> {
            product.productCategory = [];
            for(var p in product.category){
             product.productCategory.push(
                this.af.database.object('/category/' + p)
              )
            }
          }
        );
        return products;
      }
    );
    return this.productsObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
    getProducts1(){
          this.productsObservable1 = this.af.database.list('/product/');
      return this.productsObservable1; 
   }
  constructor(private af:AngularFire) { }
  deleteproduct(product){
let productKey = product.$key;
this.af.database.object('/product/' + productKey).remove();    
   }
}
