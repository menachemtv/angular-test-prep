import { TestpreperationPage } from './app.po';

describe('testpreperation App', function() {
  let page: TestpreperationPage;

  beforeEach(() => {
    page = new TestpreperationPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
